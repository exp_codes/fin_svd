import os
from collections import OrderedDict

import librosa
import numpy as np
from librosa.display import specshow
from matplotlib import pyplot as plt
from scipy.io.wavfile import read as wavread


def show_data_plot_listen_audios_spec_wav(audio_dir='audios'):
    all_wav_data = OrderedDict()
    for wav_item in os.listdir(audio_dir):
        wav_path = os.path.join(audio_dir, wav_item)
        if '.wav' in wav_path:
            sr, wav_data = wavread(wav_path)
            wav_name = wav_item.strip('.wav')
            all_wav_data[wav_name] = wav_data

    fig, axs = plt.subplots(7, 4)
    fig.subplots_adjust(hspace=.5, wspace=.001)
    axs = axs.ravel()
    plot_num = 0
    for item in all_wav_data.keys():
        chn_data = all_wav_data[item]
        axs[plot_num].plot(chn_data)
        axs[plot_num].axis('off')
        axs[plot_num].set_title(item)
        plot_num += 1

    fig.savefig('img/listen_wav.svg', format="svg", bbox_inches='tight')

    fig = plt.figure()
    fig.subplots_adjust(hspace=.5, wspace=.05)

    plot_num = 0
    for item in all_wav_data.keys():
        chn_data = all_wav_data[item]

        D = librosa.amplitude_to_db(np.abs(librosa.stft(np.asfortranarray(chn_data))), ref=np.max)
        plt.subplot(7, 4, plot_num + 1)
        specshow(D, y_axis='log')
        plt.axis('off')
        plt.title(item)
        plot_num+=1

    fig.savefig('img/listen_spec.svg', format="svg", bbox_inches='tight')

    return 0


def plot_mfcc_feat():
    y, sr = librosa.load('../test.wav', offset=30, duration=30)

    fig = plt.figure(figsize=(10, 4))
    plt.plot(y)
    plt.axis('off')
    plt.tight_layout()
    plt.show()
    fig.savefig('img/mfcc_feat_y.png', format="png", bbox_inches='tight')

    mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=20)
    fig=plt.figure(figsize=(10, 4))
    # librosa.display.specshow(mfccs, x_axis='time')
    librosa.display.specshow(mfccs)
    # plt.colorbar()
    # plt.title('MFCC')
    plt.tight_layout()
    plt.show()
    fig.savefig('img/mfcc_feat.png', format="png",bbox_inches='tight')

    return 0

if __name__ == '__main__':
    # show_data_plot_listen_audios_spec_wav()
    plot_mfcc_feat()
